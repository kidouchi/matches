# All functions in the profile page are found here

import json
import sys
import urllib

from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.shortcuts import render, redirect, get_object_or_404

# Decorator to use built-in authentication system
from django.contrib.auth.decorators import login_required

# Used to create and manually log in a user
from django.contrib.auth.models import User

# To manually create HttpResponses or raise Http404 exception
from django.http import HttpResponse, Http404

from matches_app.models import *
from matches_app.forms import *

# Return rendered profile page
@login_required
def profile(request, id):
	curr_profile = get_object_or_404(Personal, id=id)
	you = get_object_or_404(Personal, user=request.user)
	is_friend = you.friends.all().filter(id=curr_profile.id).exists()
	all_friends = you.friends.all()
	context = {
    	'is_user' : request.user.id == curr_profile.user.id,
        'profile' : curr_profile,
        'you' : you,
        'is_friended' : is_friend,
        'friends' : all_friends
    }
	return render(request, 'profile.html', context)

# Adds greeting message to user's greeting list
@login_required
def add_greeting(request):
	print >>sys.stderr, 'added greeting'
	user = get_object_or_404(User, id=request.POST['user_id'])
	send_to = get_object_or_404(Personal, user=user)

	greeting = Greeting(user=request.user, message=request.POST['message'])
	greeting.save()

	send_to.greetings.add(greeting)

	return HttpResponse("OK")

# Removes greeting message from user's greeting list
@login_required
def remove_greeting(request):
	print >>sys.stderr, request.POST['greeting_id']
	greeting = get_object_or_404(Greeting, id=request.POST['greeting_id'])
	personal = get_object_or_404(Personal, user=request.user)

	personal.greetings.remove(greeting)
	greeting.delete()

	return HttpResponse("OK")

# Get list of greetings for user
@login_required
def get_greetings(request):
	personal = get_object_or_404(Personal, user=request.user)
	data = {}
	data['greetings'] = [{'username': greeting.user.username, 
                          'message' : greeting.message,
                          'id': greeting.user.id,
                          'gid': greeting.id}
                            for greeting in personal.greetings.all()]
	return HttpResponse(json.dumps(data), content_type="application/json")

# Add friend to user's friend list
@login_required
def add_friend(request):
	you = get_object_or_404(Personal, user=request.user)
	friend = get_object_or_404(Personal, id=request.POST['friend_id'])
	you.friends.add(friend)

	return HttpResponse("OK")

# Remove friend from user's friend list
@login_required
def remove_friend(request):
	you = get_object_or_404(Personal, user=request.user)
	friend = get_object_or_404(Personal, id=request.POST['friend_id'])
	you.friends.remove(friend)

	return HttpResponse("OK")


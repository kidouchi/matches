// Get album information from Spotify API
var getAlbum = function(albumId, callback) {
	$.ajax({
		url: 'https://api.spotify.com/v1/albums/' + albumId,
		async: false,
		success: function(response) {
			callback(response);
		}
	});
};

// Process album information from Spotify API
var getAlbumsInfo = function(albums) {
	var albumsArr = [];
	var arr = [];
	var rowNum = 0;
	// Create rows of albums
	$.each(albums, function(i) {
		if (i % 3 == 0) {
			rowNum++;
			$("#album-rows").append('<div class="row placeholders" id="album-row'+rowNum+'"></div>');
		}
		var albumImg = this.images[1].url;
		var albumId = this.id;
		var albumName = this.name;
		getAlbum(albumId, function(data) {
			var spotify_url = data.external_urls.spotify;
			var albumArtist = data.artists[0].name;
			var artistId = data.artists[0].id;
			// Get album html template
			$.ajax({
				type: "POST",
				url: "/get/album/template",
				async: false,
				data: {
					album_num : i,
					album_name : albumName,
					album_img : albumImg,
					album_id : albumId,
					album_artist : albumArtist,
					artist_id : artistId,
					col_size : 4,
					spotify_url : spotify_url
				},
				success: function(response) {
					albumsArr.push(response);
				}
			});
		});
	});
	return albumsArr;
};

$(document).ready(function() {
	console.log("ready!");	
	$("#query-form").on("submit", 
		function(event) {				
			console.log("Search submitted!");
			event.preventDefault();
			// Query search from Spotify API search function
			$.ajax({
				url : 'https://api.spotify.com/v1/search',
				data : {
					q: $("#query").val(),
					type: 'album',
					limit: 50
				},
				success: function(response) {
					var albums = response.albums.items;
					var rowNum = 0;
					$("#album-rows").empty();					
					$.each(getAlbumsInfo(albums), function(i) {
						if (i % 3 == 0) {
							rowNum++;
						}
						$("#album-row" + rowNum).append(this);
					});
				},
				error: function( xhr, status, errorThrown ) {
					console.log( "Sorry, there was a problem!" );
					console.log( "Error: " + errorThrown );
					console.log( "Status: " + status );
					console.dir( xhr );
				},
				complete: function() {	
					// Favoriting a song
					$(document).on("click", ".red-heart", function(event) {
						var albumId = $(this).attr("data-album-id");
						console.log("clicked gray heart");
						getAlbum(albumId, function(data) {
							$.ajax({
								type: "POST",
								url: "/music/favorited/song",
								data: {
									singer : data.artists[0].name,
									singer_id : data.artists[0].id,
									song : data.name,
									song_id : albumId
								},
								crossDomain: true
							});
						});
						$(this).toggleClass("red-heart my-liked");
					});

					// Unfavoriting a song
					$(document).on("click", ".my-liked", function(event) {
						var albumId = $(this).attr("data-album-id");
						console.log("clicked red heart");
						$.ajax({
							type: "POST",
							url: "/music/unfavorited/song",
							data: {
								song_id : albumId
							},										
							crossDomain: true
						});
						$(this).toggleClass("my-liked red-heart");
					});		

					// Following a singer
					$(document).on("click", ".singer-follow", function(event) {
						console.log("Following artist");
						var singer = $(this).attr("data-artist-name");
						var singerId = $(this).attr("data-artist-id");
						$.ajax({
							type: "POST",
							url: "/music/favorited/singer",
							data: {
								singer: singer,
								singer_id: singerId
							},
							crossDomain: true
						});
						$(this).toggleClass("singer-follow singer-unfollow");
						$(this).html('Unfollow '+ singer);
					});

					// Unfollowing a singer
					$(document).on("click", ".singer-unfollow", function(event) {
						console.log("Unfollowing artist");
						var singer = $(this).attr("data-artist-name");
						var singerId = $(this).attr("data-artist-id");
						$.ajax({
							type: "POST",
							url: "/music/unfavorited/singer",
							data: {
								singer_id: singerId
							},
							crossDomain: true
						});
						$(this).toggleClass("singer-unfollow singer-follow");
						$(this).html('Follow '+ singer);
					});

					// Play music 
					var audioObject;
					var prevObject;
					$(".play-btn").click(function(event) {
						if (audioObject != null) {
							audioObject.pause();											
						}
						var albumId = $(this).attr("data-album-id");
						var playObj = $(this);
						prevObject = playObj;
						getAlbum(albumId, function(data) {
							audioObject = new Audio(data.tracks.items[0].preview_url);
							if (playObj.attr("playing") == "false") {
								audioObject.play();
								playObj.attr("playing", "true");
								console.log('click once');
							} else {
								audioObject.pause();
								playObj.attr("playing", "false");
								console.log('clicked again');
							}
						});	
					});

					// Info Tooltip
					$(".info-icon").hover(function(event) {
						var pos = $(this).position();
					    $($(this).data("tooltip")).css({
					        left: pos.left-250,
					        top: pos.top+30,
					        width: "75%"
					    }).stop().show(100);
					}, function() {
					    $($(this).data("tooltip")).hide();
					});
					$(".info-tip").hover(function(event){
						$(this).show();
					}, function() {
						$(this).hide();
					});
					
					// Fade in search animation
					var time = 0;
					$("div#album-col").each(function(i) {
						$(this).hide();
						$(this).delay(time).fadeIn("slow");
						time += 300;
					});
				}
			});
	});
});



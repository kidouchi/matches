$(document).ready(function() { 
	$.ajax({
		type: "POST",
		url: "/get/greetings",
		dataType: "json",
		success: function(response) {
			console.log(response);
			$.each(response.greetings, function() {
				var gid = this.gid;
				var uid = this.id;
				(new PNotify({
				    title: this.username + ' greeted you!',
				    text: 'Message: "' + this.message + '"\nWould you like to reply?', 
				    icon: 'glyphicon glyphicon-envelope',
				    hide: false,
				    confirm: {
				    	prompt: true
				    },
				    buttons: {
				    	closer: false,
				    	sticker: false
				    },
				    history: {
				    	history: false
				    }
				    })).get().on('pnotify.confirm', function(e, notice, val) {
						var newMsg = $('<div/>').text(val).html();
						$.ajax({
							type: "POST",
							url: "/greeting/add",
							data: {
								message : newMsg,
								user_id : uid
							}
						});

						$.ajax({
				    		type: "POST",
				    		url: "/greeting/remove",
				    		data: {
				    			greeting_id : gid
				    		}
				    	});

				    	notice.cancelRemove().update({
				    		title: ' The greeting was sent!',
				    		text: '',
				    		icon: true,
				    		type: 'info',
				    		hide: true,
				    		confirm: {
				    			prompt: false
				    		},
				    		buttons: {
				    			closer: true,
				    			sticker: true
				    		}
				    	});
				    }).on('pnotify.cancel', function(e, notice) {
				    	$.ajax({
				    		type: "POST",
				    		url: "/greeting/remove",
				    		data: {
				    			greeting_id : gid
				    		}
				    	});
				    	notice.cancelRemove().update({
				    		title: ' That\'s too bad :(',
				    		text: '',
				    		icon: true,
				    		type: 'info',
				    		hide: true,
				    		confirm: {
				    			prompt: false
				    		},
				    		buttons: {
				    			closer: true,
				    			sticker: true
				    		}
				    	});
					});
			});
		}
	});

});

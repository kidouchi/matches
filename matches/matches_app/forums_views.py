# All functionality in the forums are found here

import json
import sys
import urllib
from itertools import chain

from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.shortcuts import render, redirect, get_object_or_404

# Decorator to use built-in authentication system
from django.contrib.auth.decorators import login_required

# Used to create and manually log in a user
from django.contrib.auth.models import User

# To manually create HttpResponses or raise Http404 exception
from django.http import HttpResponse, Http404

from matches_app.models import *
from matches_app.forms import *

# Return rendered forums page
@login_required
def to_forum(request):
	personal = get_object_or_404(Personal, user=request.user)
	context = {'personal': personal}
	return render(request, 'forums.html', context)

# Return rendered page for when users want to make a new forum topic
@login_required
def new_topic(request):
	personal = get_object_or_404(Personal, user=request.user)
	context = {'personal' : personal}
	return render(request, 'new-topic.html', context)

# Save the new forum topic
@login_required
def new_forum(request):
	personal = get_object_or_404(Personal, user=request.user)
	new_forum = Forum(title = request.POST['title'], 
						info = request.POST['topic'],
						who_posted = personal)
	new_forum.save()
	for tag in request.POST['tags'].split(","):
		newTag = Tag(tag_name=tag)
		newTag.save()
		new_forum.tags.add(newTag)

	return redirect(reverse('forums'))

# Get list of all forums made
@login_required
def get_forums(request):
	forums = Forum.objects.all().order_by('-posted_at') 
	context = {'forums' : forums}
	return render(request, 'forums-list.html', context)

# Get a specific forum
@login_required
def get_forum(request, id):
	forum = get_object_or_404(Forum, id=id)	
	context = {'forum' : forum}
	return render(request, 'forum-inside.html', context)

# Add comment to forum's comment list
@login_required
def add_comment(request):
	personal = get_object_or_404(Personal, user=request.user)
	new_comment = Comment(who_commented=personal,
						  comment=request.POST['comment'])
	new_comment.save()

	forum = get_object_or_404(Forum, id=request.POST['forum_id'])
	forum.comments.add(new_comment)

	context = {'comment' : new_comment, 'forum' : forum}
	return render(request, 'comment-template.html', context)

# Add reponse comment to forum's comment list
@login_required
def add_response(request):
	personal = get_object_or_404(Personal, user=request.user)
	forum = get_object_or_404(Forum, id=request.POST['forum_id'])
	comment = forum.comments.all().get(id=request.POST['comment_id'])

	response = Comment(who_commented=personal,
						comment=request.POST['response'],
						replied_comment=comment)
	response.save()
	forum.comments.add(response)

	context = {'response' : response, 'forum' : forum}
	return render(request, 'comment-reply-template.html', context)

# Returns list of forums that match query
@login_required
def forum_search(request):
	search_results = []
	keyword = request.POST['search']
	if (len(request.POST.getlist('type')) == 0):
		return get_forums(request)

	for type_res in request.POST.getlist('type'):
		if (type_res == 'title'):
			title_results = Forum.objects.filter(title__contains=keyword)
			search_results = list(chain(search_results, title_results))

		if (type_res == 'tag'):
			tag_results = Forum.objects.filter(tags__tag_name__exact=keyword)
			search_results = list(chain(search_results, tag_results))

		if (type_res == 'content'):
			content_results = Forum.objects.filter(info__contains=keyword)
			search_results = list(chain(search_results, content_results))

		if (type_res == 'user'):
			user = get_object_or_404(User, username=keyword)
			personal = get_object_or_404(Personal, user=user)
			user_results = Forum.objects.filter(who_posted=personal)
			search_results = list(chain(search_results, user_results))

	context = {'forums' : set(sorted(search_results))}
	return render(request, 'forums-list.html', context)




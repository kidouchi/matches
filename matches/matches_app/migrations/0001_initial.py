# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment', models.CharField(max_length=200)),
                ('commented_at', models.DateTimeField(auto_now_add=True)),
                ('replied_comment', models.ForeignKey(related_name=b'test', to='matches_app.Comment')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Forum',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('posted_at', models.DateTimeField(auto_now_add=True)),
                ('title', models.CharField(max_length=100)),
                ('info', models.CharField(max_length=400)),
                ('comments', models.ManyToManyField(to='matches_app.Comment')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Greeting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('message', models.CharField(max_length=100)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Personal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('intro', models.CharField(max_length=42, blank=True)),
                ('create_time', models.DateTimeField(auto_now_add=True)),
                ('picture', models.ImageField(upload_to=b'user-photos', blank=True)),
                ('birthday', models.DateTimeField(max_length=42, null=True, blank=True)),
                ('location', models.CharField(max_length=42)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Singer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('singer_id', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Song',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('song_id', models.CharField(max_length=100)),
                ('singer', models.ForeignKey(to='matches_app.Singer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tag_name', models.CharField(max_length=42)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='personal',
            name='emotion_song',
            field=models.OneToOneField(null=True, blank=True, to='matches_app.Song'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='personal',
            name='favorite_singers',
            field=models.ManyToManyField(to='matches_app.Singer'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='personal',
            name='favorite_songs',
            field=models.ManyToManyField(related_name=b'favorite_song', to='matches_app.Song'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='personal',
            name='friends',
            field=models.ManyToManyField(to='matches_app.Personal'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='personal',
            name='greetings',
            field=models.ManyToManyField(to='matches_app.Greeting'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='personal',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='forum',
            name='tags',
            field=models.ManyToManyField(to='matches_app.Tag'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='forum',
            name='who_posted',
            field=models.ForeignKey(to='matches_app.Personal'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='comment',
            name='who_commented',
            field=models.ForeignKey(to='matches_app.Personal'),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matches_app', '0006_auto_20141201_0114'),
    ]

    operations = [
        migrations.AlterField(
            model_name='personal',
            name='birthday',
            field=models.DateField(max_length=42, null=True, blank=True),
        ),
    ]

from django.conf.urls import patterns, include, url
from matches_app.forms import LoginForm
from django.views.generic.base import TemplateView

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
		url(r'^$', 'matches_app.views.home', name='home'),
		url(r'^edit-user/(?P<id>\d+)$', 'matches_app.views.edit_user', name='edit-user'),
		url(r'^photo/(?P<id>\d+)$', 'matches_app.views.get_photo', name='photo'),
		url(r'^match$', 'matches_app.views.match',name='match'),
		# Profile
		url(r'^profile/(?P<id>\d+)$', 'matches_app.profile_views.profile', name='profile'),
		url(r'^profile/friend/add$', 'matches_app.profile_views.add_friend', name='add-friend'),
		url(r'^profile/friend/remove$', 'matches_app.profile_views.remove_friend', name='remove-friend'),
		# Music Template
		url(r'^music/search$', 'matches_app.views.music_search', name='music-search'),
		url(r'^get/album/template$', 'matches_app.views.album_template', name='album-template'),
		# Login/Registration/Recover PW
		url(r'^register$', 'matches_app.views.register',name='register'),
		url(r'^fbregister$', 'matches_app.views.fbregister',name='fbregister'),
		url(r'^reset-password$', 'matches_app.views.resetpassword',name='resetpassword'),
		url(r'^login$', 'django.contrib.auth.views.login', {'template_name':'login.html', 
			'authentication_form' : LoginForm}, name='login'),
		url(r'^logout$', 'django.contrib.auth.views.logout_then_login',name='logout'),
		url(r'^confirm-registration/(?P<username>[a-zA-Z0-9_@\+\-]+)/(?P<token>[a-z0-9\-]+)$', 'matches_app.views.confirm_registration', name='confirm'),
		url(r'^confirm-password/(?P<username>[a-zA-Z0-9_@\+\-]+)/(?P<token>[a-z0-9\-]+)$', 'matches_app.views.confirm_password', name='confirmpossword'),
		url(r'^recover-password$', 'matches_app.views.recoverpassword',name='recoverpassword'),
		# Greeting
		url(r'^greeting/add$', 'matches_app.profile_views.add_greeting', name='add-greeting'),
		url(r'^greeting/remove$', 'matches_app.profile_views.remove_greeting', name='remove-greeting'),
		url(r'^get/greetings$', 'matches_app.profile_views.get_greetings', name='get-greetings'),
		# Forums
		url(r'^forums$', 'matches_app.forums_views.to_forum', name='forums'),
		url(r'^forums/new/topic$', 'matches_app.forums_views.new_topic', name='new-topic'),
		url(r'^forums/done$', 'matches_app.forums_views.new_forum', name='new-forum'),
		url(r'^forums/list$', 'matches_app.forums_views.get_forums', name='forums-list'),
		url(r'^forum/(?P<id>\d+)$', 'matches_app.forums_views.get_forum', name='inside-forum'),
		url(r'^forum/comment/add$', 'matches_app.forums_views.add_comment', name='add-comment'),
		url(r'^forum/comment/respond$', 'matches_app.forums_views.add_response', name='add-response'),
		url(r'^forum/search$', 'matches_app.forums_views.forum_search'),
		# Favoriting Song
		url(r'^music/favorited/song$', 'matches_app.favorited_views.favorited_song'),
		url(r'^music/unfavorited/song$', 'matches_app.favorited_views.unfavorited_song'),
		url(r'^music/favorite/songs$', 'matches_app.favorited_views.get_favorited_songs'),
		# Favoriting Singer
		url(r'^music/favorited/singer$', 'matches_app.favorited_views.favorited_singer'),
		url(r'^music/unfavorited/singer$', 'matches_app.favorited_views.unfavorited_singer'),
		url(r'^music/favorite/singers$', 'matches_app.favorited_views.get_favorited_singers'),
		# Get Recent Favorite Songs
		url(r'^music/recent/favorites$', 'matches_app.favorited_views.get_recent_favorites'),
		url(r'^music/recent/template$', 'matches_app.favorited_views.recent_favs_template'),
		# Spotify related
		url(r'^spotify/authorize$', 'matches_app.views.authorize', name="authorize"),
		) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
